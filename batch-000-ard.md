# 230703 - Batch 000:  Academic Reading List 

### **Topics**

- ES6 Updates
  - [w3schools](https://www.w3schools.com/js/js_es6.asp)
  - [exploringjs](https://exploringjs.com/es6/ch_overviews.html)
- Selection Control
  - [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/if...else)
- For Loop Statements
  - [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for)
- NodeJS
  - [W3Schools](https://www.w3schools.com/nodejs/nodejs_intro.asp)
- NodeJS HTTP Module
  - [W3Schools](https://www.w3schools.com/nodejs/nodejs_http.asp)


### **Purpose**

- Create Javascript functions using the arrow function syntax, perform array and object destructuring.
- Create a Javascript For statements
- Create a simple server using the HTTP module.


### **Goal to Checking**

1. Create a student grading system using an arrow function. The grade categories are as follows:
   - Failed (74 and below)
   - Beginner (75-80)
   - Developing (81-85)
   - Above Average (86-90)
   - Advanced (91-100)

2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even.

    Sample output in the console: <br>
        1 - odd <br>
        2 - even <br>
        3 - odd <br>
        4 - even <br>
        5 - odd <br>
        etc.

3. Create a simple server using the HTTP module.

    a. Create an `index.js` file inside the `readingListActC` folder.

    b. Import the `http` module using the `require` directive. Create a variable `port` and assign it the value `8000`.

    c. Create a server using the `createServer` method that will listen to the port provided above.

    d. Console log a message in the terminal when the server is successfully running.

    e. Create a condition that prints a specific message when the following routes are accessed: login, register, homepage, and usersProfile.

    f. Test each route to ensure they are working as intended. Save a screenshot for each test.

    g. Create a condition for any other routes that will return an error message.


